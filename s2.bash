#!/bin/bash

TODAY=$(date +"%Y-%m-%d")

echo "Enter your Firstname:"
read Firstname

echo "Enter your Lastname:"
read Lastname

echo "Enter Birthdate(yyyy-mm-dd):"
read Birthdate

tmpDays=$( printf '%s' $(( $(date -u -d"$TODAY" +%s) - $(date -u -d"$Birthdate" +%s))) )
age=$(( $tmpDays / 60 / 60 / 24 / 364 ))
echo "Hello, $Firstname $Lastname! You're $age years old!"